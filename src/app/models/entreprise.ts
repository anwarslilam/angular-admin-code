export class Entreprise{
    id: number;
    name: string;
    email: string;
    phone: string;
    pays: string;
    statut: string;
}