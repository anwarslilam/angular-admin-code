import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { KeycloakSecurityService } from './keycloak-security.service';
import { throwError } from 'rxjs';
import { tap, catchError, map } from 'rxjs/operators';
import { Entreprise } from '../models/entreprise';

@Injectable({
  providedIn: 'root'
})
export class EntrepriseService {

  headers: HttpHeaders =new HttpHeaders;
  options : any;

  constructor(
    private http: HttpClient, 
    private keycloakSecurityService: KeycloakSecurityService
    ) { 
      this.headers.append('enctype', 'multipart/form-data');
      this.headers.append('Content-Type', 'application/json');
      this.headers.append('X-requested-with', 'XMLHttpRequest');
      this.options = {headers: this.headers};
    }

    public getEntreprises() {
      return this.http.get<Entreprise[]>('http://localhost:8081/getEntreprises', this.options)
      .pipe(
          map(res =>res),
          catchError(this.handleError)
        );
    }

    public getEntreprise(id) {
      return this.http.get<Entreprise>('http://localhost:8081/getEntreprise/'+id)
      .pipe(
          map(res =>res),
          catchError(this.handleError)
        );
    }

    public CreateEntreprise(entreprise, user) {
      return this.http.post<any>('http://localhost:8081/createEntreprise', {entreprise, user}, this.options)
      .pipe(
          map(res =>res),
          catchError(this.handleError)
        );
    }

    public UpdateEntreprise(id : number, entreprise : Entreprise) {
      return this.http.put<any>('http://localhost:8081/updateEntreprise/'+id, entreprise, this.options)
      .pipe(
          map(res =>res),
          catchError(this.handleError)
        );
    }

    public DeleteEntreprise(id : number) {
      return this.http.delete<any>('http://localhost:8081/deleteEntreprise/'+id, this.options)
      .pipe(
          map(res =>res),
          catchError(this.handleError)
        );
    }
  
  
  
    private handleError(errorRes: HttpErrorResponse) {
      console.log('errorRes', errorRes)
      let errorMessage = 'an unknown error occured';
      if (!errorRes.error || !errorRes.error.error || !errorRes.error.message) {
        return throwError(errorMessage);
      }
      switch (errorRes.error.message) {
        case 'Forbidden':
          errorMessage = 'Vous n\'avez pas les droits ! ou vous n\'êtes pas logger';
          break;
        case 'EMAIL_NOT_FOUND':
          errorMessage = 'this Email does not exist';
          break;
        case 'INVALID_PASSWORD':
          errorMessage = 'this password is not correct';
          break;
  
      }
      return throwError(errorMessage);
    }
}
