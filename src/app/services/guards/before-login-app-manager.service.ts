import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { KeycloakSecurityService } from '../keycloak-security.service';

@Injectable()
export class BeforeLoginAppManagerService implements CanActivate {

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | Observable<boolean> | Promise<boolean> {
    if(!this.keycloakSecurityService.isAuth) {
      return true;
    }
    else {
      if(this.keycloakSecurityService.isAppManager()){
         this.router.navigateByUrl('dashboard');
         return false;
      }
      else{
        window.location.href = 'http://localhost:4200/home';
        return false;
      }
          
    }
  }
  constructor(
    private keycloakSecurityService: KeycloakSecurityService,
    private router: Router
    ) { }

}
