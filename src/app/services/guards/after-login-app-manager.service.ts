import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { KeycloakSecurityService } from '../keycloak-security.service';

@Injectable()
export class AfterLoginAppManagerService implements CanActivate {
  

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | Observable<boolean> | Promise<boolean> {
    if(this.keycloakSecurityService.isAuth) {
      if(this.keycloakSecurityService.isAppManager()){
        return true;
      }
      else{
        window.location.href = 'http://localhost:4200/home';
        return false;
      } 
    }
    else {
         this.router.navigateByUrl('login');
         return false;
    }
  }
  constructor(
    private keycloakSecurityService: KeycloakSecurityService,
    private router: Router
    ) { }

}
