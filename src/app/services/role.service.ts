import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { throwError } from 'rxjs';
import { tap, catchError, map } from 'rxjs/operators';
import { Role } from '../models/role';

@Injectable({
  providedIn: 'root'
})
export class RoleService {

  headers: HttpHeaders =new HttpHeaders;
  options : any;

  constructor(
    private http: HttpClient
    ) { 
      this.headers.append('enctype', 'multipart/form-data');
      this.headers.append('Content-Type', 'application/json');
      this.headers.append('X-requested-with', 'XMLHttpRequest');
      this.options = {headers: this.headers};
    }

    public getRoles() {
      return this.http.get<Role[]>('http://localhost:8081/getRoles', this.options)
      .pipe(
          map(res =>res),
          catchError(this.handleError)
        );
    }

    public getRole(id) {
      return this.http.get<Role>('http://localhost:8081/getRole/'+id)
      .pipe(
          map(res =>res),
          catchError(this.handleError)
        );
    }

    public CreateRole(role) {
      return this.http.post<any>('http://localhost:8081/createRole', role, this.options)
      .pipe(
          map(res =>res),
          catchError(this.handleError)
        );
    }

    public UpdateRole(name : string, role : Role) {
      return this.http.put<any>('http://localhost:8081/updateRole/'+name, role, this.options)
      .pipe(
          map(res =>res),
          catchError(this.handleError)
        );
    }

    public DeleteRole(name : string) {
      return this.http.delete<any>('http://localhost:8081/deleteRole/'+name, this.options)
      .pipe(
          map(res =>res),
          catchError(this.handleError)
        );
    }
  
  
  
    private handleError(errorRes: HttpErrorResponse) {
      console.log('errorRes', errorRes)
      let errorMessage = 'an unknown error occured';
      if (!errorRes.error || !errorRes.error.error || !errorRes.error.message) {
        return throwError(errorMessage);
      }
      switch (errorRes.error.message) {
        case 'Forbidden':
          errorMessage = 'Vous n\'avez pas les droits ! ou vous n\'êtes pas logger';
          break;
        case 'EMAIL_NOT_FOUND':
          errorMessage = 'this Email does not exist';
          break;
        case 'INVALID_PASSWORD':
          errorMessage = 'this password is not correct';
          break;
  
      }
      return throwError(errorMessage);
    }
}
