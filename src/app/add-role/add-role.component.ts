import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Role } from '../models/role';
import { MessageService } from '../services/message.service';
import { RoleService } from '../services/role.service';

@Component({
  selector: 'app-add-role',
  templateUrl: './add-role.component.html',
  styleUrls: ['./add-role.component.scss']
})
export class AddRoleComponent implements OnInit {

  constructor(
    private roleService: RoleService,
    private messageService: MessageService
  ) { }

  role = new  Role();
  errorMessage: string = null;
  comp = "#roles";

  ngOnInit(): void {
  }

  addRole() {
    this.roleService
        .CreateRole(this.role)
        .subscribe(
          response => {
            console.log(response);
            this.messageService.showMessage("div#msg","alert-info",response["message"], "glyphicon-ok");
        },
          err => {
            this.errorMessage = err;
            console.log('errorrr ! ', err)
          })
  }

}
