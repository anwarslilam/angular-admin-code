import { Component, OnInit } from '@angular/core';
import { KeycloakSecurityService } from '../services/keycloak-security.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor(private keycloakSecurityService: KeycloakSecurityService) { }


  ngOnInit(): void {
      this.keycloakSecurityService.login();
  }

}
