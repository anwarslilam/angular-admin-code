import { Component, OnInit } from '@angular/core';
import { EntrepriseService } from '../services/entreprise.service';
import { MessageService } from '../services/message.service';


@Component({
  selector: 'app-entreprises',
  templateUrl: './entreprises.component.html',
  styleUrls: ['./entreprises.component.scss']
})
export class EntreprisesComponent implements OnInit {

  errorMessage: string = null;
  entreprises: any;
  comp = "#entreprises";

  constructor(
    private entrepriseService: EntrepriseService,
    private messageService: MessageService) { }

  ngOnInit(): void {
    this.getEntreprises();
  }

  getEntreprises(){
    this.entrepriseService.getEntreprises().subscribe(
      data => {
        this.entreprises = data;
        this.entreprises.sort((a,b) => (a.id > b.id) ? -1 : ((b.id > a.id) ? 1 : 0));
      },
      err => {
        this.errorMessage = err;
        console.log('errorrr ! ', err)
      }
    );
  }

  deleteEntreprise(id) {
    var response=confirm("Are you sure you want to delete this entreprise?");
    if(response==true) {
      this.entrepriseService.DeleteEntreprise(id).subscribe(
        data => {
        console.log(data);
          this.getEntreprises();
          this.messageService.showMessage("div#msg","alert-info","Entreprise has been DELETED.","glyphicon-ok")
      },
        err => {
          console.log('error deleting entreprise ! ', err)
        }
      );
    } else { //cancel button
    }
  }

}
