import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { EntrepriseService } from '../services/entreprise.service';
import { Entreprise } from '../models/entreprise';
import { MessageService } from '../services/message.service';

@Component({
  selector: 'app-edit-entreprise-statut',
  templateUrl: './edit-entreprise-statut.component.html',
  styleUrls: ['./edit-entreprise-statut.component.scss']
})
export class EditEntrepriseStatutComponent implements OnInit {

  constructor(
    private route: ActivatedRoute,
    private entrepriseService: EntrepriseService,
    private messageService: MessageService
  ) { }


  entreprise = new  Entreprise();
  errorMessage: string = null;
  comp = "#entreprises";
  status = [
    { statut: 'en traitement'},
    { statut: 'accepté'},
    { statut: 'réfusé'}
  ];


  ngOnInit(): void {
    this.getEntreprise();
  }

  getEntreprise() {
    var id = this.route.snapshot.params['id'];
    this.entrepriseService
        .getEntreprise(id)
        .subscribe(
          data => {
            this.entreprise = data;
          },
          err => {
            this.errorMessage = err;
            console.log('error getting entreprise ! ', err)
          })
  }

  updateEntreprise() {
    this.entrepriseService
        .UpdateEntreprise(this.entreprise.id, this.entreprise)
        .subscribe(
          response => {
            console.log(response);
            this.messageService.showMessage("div#msg","alert-info","entreprise has been successfully updated.", "glyphicon-ok");
        },
        err => {
          this.errorMessage = err;
          console.log('error updating entreprise ! ', err)
        })
  }

}
