import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { EntreprisesComponent } from './entreprises/entreprises.component';
import { LoginComponent } from './login/login.component';
import { BeforeLoginAppManagerService } from './services/guards/before-login-app-manager.service';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AfterLoginAppManagerService } from './services/guards/after-login-app-manager.service';
import { EditEntrepriseStatutComponent } from './edit-entreprise-statut/edit-entreprise-statut.component';
import { RolesComponent } from './roles/roles.component';
import { AddRoleComponent } from './add-role/add-role.component';
import { EditRoleComponent } from './edit-role/edit-role.component';




const routes: Routes = [
  {path: '', redirectTo: 'login', pathMatch:'full'},
  {path: 'entreprises', canActivate: [AfterLoginAppManagerService], component:EntreprisesComponent},
  {path: 'login', canActivate: [BeforeLoginAppManagerService], component:LoginComponent},
  {path: 'dashboard', canActivate: [AfterLoginAppManagerService], component:DashboardComponent},
  {path: 'edit-entreprise-statut/:id', canActivate: [AfterLoginAppManagerService], component:EditEntrepriseStatutComponent},
  {path: 'roles', canActivate: [AfterLoginAppManagerService], component:RolesComponent},
  {path: 'add-role', canActivate: [AfterLoginAppManagerService], component:AddRoleComponent},
  {path: 'edit-role/:id', canActivate: [AfterLoginAppManagerService], component:EditRoleComponent},

];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule],
  declarations: []
})

export class AppRoutingModule { }