import { BrowserModule } from '@angular/platform-browser';
import { DoBootstrap, NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { KeycloakSecurityService } from './services/keycloak-security.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { BeforeLoginAppManagerService } from './services/guards/before-login-app-manager.service';
import { AfterLoginAppManagerService } from './services/guards/after-login-app-manager.service';
import { MessageService } from './services/message.service';
import { RequestInterceptorService } from './services/request-interceptor.service';
import { LoginComponent } from './login/login.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { NavComponent } from './nav/nav.component';
import { EditEntrepriseStatutComponent } from './edit-entreprise-statut/edit-entreprise-statut.component';
import { NavbarComponent } from './navbar/navbar.component';
import { EntreprisesComponent } from './entreprises/entreprises.component';
import { RolesComponent } from './roles/roles.component';
import { AddRoleComponent } from './add-role/add-role.component';
import { EditRoleComponent } from './edit-role/edit-role.component';
import { EntrepriseService } from './services/entreprise.service';
import { RoleService } from './services/role.service';

export function keycloakFactory(keycloakSecurityService: KeycloakSecurityService) {
  return () => keycloakSecurityService.init();
}

const keycloakSecurityService = new KeycloakSecurityService();

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    DashboardComponent,
    NavComponent,
    EditEntrepriseStatutComponent,
    NavbarComponent,
    EntreprisesComponent,
    RolesComponent,
    AddRoleComponent,
    EditRoleComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
  ],
  providers: [
    BeforeLoginAppManagerService,
    AfterLoginAppManagerService,
    EntrepriseService,
    RoleService,
    MessageService,
   // { provide: APP_INITIALIZER, deps: [KeycloakSecurityService], useFactory: keycloakFactory, multi: true },
    { provide: KeycloakSecurityService, useValue: keycloakSecurityService },
    { provide: HTTP_INTERCEPTORS, useClass: RequestInterceptorService, multi: true }
  ],
  entryComponents: [AppComponent],
  //bootstrap: [AppComponent]
})

export class AppModule implements DoBootstrap {
  ngDoBootstrap(appRef: import("@angular/core").ApplicationRef): void {
    keycloakSecurityService.init().then(data => {
      console.log('authenticated + token :', data);
      appRef.bootstrap(AppComponent);

    }).catch(err => {
      console.error('err', err);
    });
  }
}
