import { Component, Input, OnInit } from '@angular/core';
import * as $ from "jquery";

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss']
})
export class NavComponent implements OnInit {

  @Input () comp="" ;

  constructor() { }

  ngOnInit(): void {
    $('li').removeClass("active");
    $(this.comp).addClass("active");
  }

}
