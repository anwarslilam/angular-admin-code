import { Component, OnInit } from '@angular/core';
import { MessageService } from '../services/message.service';
import { RoleService } from '../services/role.service';

@Component({
  selector: 'app-roles',
  templateUrl: './roles.component.html',
  styleUrls: ['./roles.component.scss']
})
export class RolesComponent implements OnInit {

  errorMessage: string = null;
  roles: any;
  comp = "#roles";

  constructor(
    private roleService: RoleService,
    private messageService: MessageService) { }

  ngOnInit(): void {
    this.getRoles();
  }

  getRoles(){
    this.roleService.getRoles().subscribe(
      data => {
        this.roles = data;
        this.roles.sort((a,b) => (a.id > b.id) ? -1 : ((b.id > a.id) ? 1 : 0));
      },
      err => {
        this.errorMessage = err;
        console.log('error getting roles ! ', err)
      }
    );
  }

  deleteRole(name) {
    var response=confirm("Are you sure you want to delete this role?");
    if(response==true) {
      this.roleService.DeleteRole(name).subscribe(
        response => {
        console.log(response);
          this.getRoles();
          this.messageService.showMessage("div#msg","alert-info",response["message"],"glyphicon-ok")
      },
        err => {
          console.log('error deleting entreprise ! ', err)
        }
      );
    } else { //cancel button
    }
  }

}
