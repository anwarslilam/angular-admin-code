import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Role } from '../models/role';
import { MessageService } from '../services/message.service';
import { RoleService } from '../services/role.service';

@Component({
  selector: 'app-edit-role',
  templateUrl: './edit-role.component.html',
  styleUrls: ['./edit-role.component.scss']
})
export class EditRoleComponent implements OnInit {

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private roleService: RoleService,
    private messageService: MessageService
  ) { }

  role = new  Role();
  errorMessage: string = null;
  comp = "#roles";

  ngOnInit(): void {
    this.getRole();
  }

  getRole() {
    var id = this.route.snapshot.params['id'];
    this.roleService
        .getRole(id)
        .subscribe(
          data => {
            this.role = data;
          },
          err => {
            this.errorMessage = err;
            console.log('error getting role ! ', err)
          })
  }

  updateRole() {
    this.roleService
        .UpdateRole(this.role.name, this.role)
        .subscribe(
          response => {
          console.log(response);
          this.messageService.showMessage("div#msg","alert-info",response["message"], "glyphicon-ok");
        },
        err => {
          this.errorMessage = err;
          console.log('error updating role ! ', err)
        })
  }

}
